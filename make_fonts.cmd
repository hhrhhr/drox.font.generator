@echo off
set DROX=f:\lalala\Drox.Operative

pushd %~dp0%

if exist "_tmp" (
    del /q /f "_tmp\*.*"
) else (
    mkdir "_tmp"
)

if exist "_out" (
    del /q /f "_out\*.*"
) else (
    mkdir "_out"
)

"tools\luajit.exe" "lua\generate_fonts.lua"

rem copy /y "_out\*.fnt" "%DROX%\Assets\Fonts"
rem copy /y "_out\*.ctx" "%DROX%\Assets\Textures\Fonts"

popd
pause
