bmfc_content = [[
# AngelCode Bitmap Font Generator configuration file
fileVersion=1

# font settings
fontName=%s
charSet=204
fontSize=%d # 48
aa=1
scaleH=100
useSmoothing=1
isBold=0
isItalic=0
useUnicode=0
disableBoxChars=1
outputInvalidCharGlyph=0
dontIncludeKerningPairs=1
useHinting=1
renderFromOutline=1
useClearType=1

# character alignment
paddingDown=0
paddingUp=0
paddingRight=0
paddingLeft=0
spacingHoriz=%d # 4
spacingVert=%d # 4
useFixedHeight=0
forceZero=1

# output file
outWidth=%d # 512
outHeight=%d # 512
outBitDepth=32
fontDescFormat=2
fourChnlPacked=0
textureFormat=tga
textureCompression=1
alphaChnl=0
redChnl=4
greenChnl=4
blueChnl=4
invA=0
invR=0
invG=0
invB=0

# outline
outlineThickness=0

# selected chars
chars=32-126,168-169,174,184-185,192-255

# imported icon images 
]]
