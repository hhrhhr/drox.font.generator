package.path = "lua/?.lua"
require("inc_fonts")
require("inc_bmfc")
require("util_binary_reader")
require("util_binary_writer")


function exist(filename)
    local f = io.open(filename)
    if f then
        f:close()
        return true
    end
    return false
end

function make_bmfc(font_name, font_size, spacing, width, height)
    local bmfc = string.format(bmfc_content,
        font_name, font_size, spacing, spacing, width, height
    )
    return bmfc
end

function trim_dds(tex_name, dds_name)
    local dds = BinaryReader
    local ctx = BinaryWriter
    dds:open(tex_name .. ".dds")
    ctx:open(dds_name .. ".ctx")

    dds:seek(12)
    local width = dds:uint32()
    local height = dds:uint32()
    dds:seek(28)
    local mips = dds:uint32()
    dds:seek(128)
    local data = dds:str(dds:size() - 128)
    --print(width, height, mips)

    ctx:uint32(width)
    ctx:uint32(height)
    ctx:uint32(4)
    ctx:uint32(mips)
    ctx:str("DXT5")
    ctx:str(data)

    dds:close()
    ctx:close()
end

function convert_fnt(fnt1_name, fnt2_name)
    local fnt1 = BinaryReader
    local fnt2 = BinaryWriter
    fnt1:open(fnt1_name)
    fnt2:open(fnt2_name)

    local tmp = fnt1:uint32()        -- "BMF" .. 0x03
    tmp = fnt1:uint8()               -- block 01
    local block_size = fnt1:uint32() -- size of block 01 (30)
    local pos = fnt1:pos()
    local font_size = fnt1:uint16()

    fnt1:seek(pos + block_size)      -- jump to block 2
    tmp = fnt1:uint8()
    block_size = fnt1:uint32()
    pos = fnt1:pos()
    local line_height = fnt1:uint16() -- line height
    local base = fnt1:uint16()       -- base
    local width = fnt1:uint16()
    local height = fnt1:uint16()

    fnt1:seek(pos + block_size)      -- jump to block 3
    tmp = fnt1:uint8()
    block_size = fnt1:uint32()
    pos = fnt1:pos()

    fnt1:seek(pos + block_size)      -- jump to block 4
    tmp = fnt1:uint8()
    block_size = fnt1:uint32()
    local num_chars = block_size / 20

    local char_indexes = {}
    for j = 0, 255 do
        table.insert(char_indexes, j)
    end

    local char_data = {}

    local fmt = "%.8f %.8f %.8f %.8f %d %d\n"
    local line = ""

    for i = 1, num_chars do
        local id = fnt1:uint32()

        local t = {}
        t.x = fnt1:uint16()
        t.y = fnt1:uint16()
        t.w = fnt1:uint16()
        t.h = fnt1:uint16()
        t.xo = fnt1:uint16()
        t.yo = fnt1:uint16()
        t.xa = fnt1:uint16()
        t.p = fnt1:uint8()
        t.c = fnt1:uint8()

        line = string.format(fmt,
            t.x / width, t.y / height, (t.x + t.w) / width, (t.y + t.h) / height, t.w, t.h)

        if id == 32 then
            for j = 0, 255 do
                table.insert(char_data, line)
            end
        else
            char_data[id + 1] = line
        end

    end


    line = "size " .. font_size .. "\r\n\r\n"
    fnt2:str(line)

    line = "charIndexes\r\n{\r\n"
    fnt2:str(line)

    line = table.concat(char_indexes, " ")
    fnt2:str(line)
    fnt2:str(" \r\n")

    line = "}\r\n\r\ncharData\r\n{\r\n"
    fnt2:str(line)

    line = table.concat(char_data)
    fnt2:str(line)

    line = "}\r\n"
    fnt2:str(line)

    fnt1:close()
    fnt2:close()
end


local bmfont = "tools\\bmfont64.exe -c \"%s\" -o \"%s\" >nul"
local convert = "tools\\magick.exe convert \"%s\" -flip \"%s\""
local nvcompress = "tools\\nvcompress.exe -alpha -bc3 \"%s\" \"%s\" >nul" -- -nomips


for _, font_name in pairs(fonts.font_names) do
    local width, height = 128, 128
    local spacing = 4

    for _, font_size in ipairs(fonts.sizes) do
        local bmfc_name = "_tmp\\" .. font_name .. font_size .. ".bmfc"
        local tex_name  = "_tmp\\" .. font_name .. font_size
        local dds_name  = "_out\\Gentium" .. font_size
        local fnt1_name  = "_tmp\\" .. font_name .. font_size .. ".fnt"
        local fnt2_name  = "_out\\Gentium" .. font_size .. ".fnt"

        while true do
            local bmfc = make_bmfc(font_name, font_size, spacing, width, height)
            local f = assert(io.open(bmfc_name, "w+b"))
            f:write(bmfc)
            f:close()

            os.execute(string.format(bmfont, bmfc_name, tex_name))
            if exist(tex_name .. "_1.tga") or exist(tex_name .. "_01.tga") then
                print("not fitted in " .. width .. "x" .. height .. " px :(")
                os.execute("del \"" .. tex_name .. "_*\"")
                if width <= height then
                    width = width * 2
                else
                    height = height * 2
                end
            else
                print("OK\n\n")
                break
            end
        end

        os.execute(string.format(convert, tex_name .. "_0.tga", tex_name .. "_flipped.tga"))

        os.execute(string.format(nvcompress, tex_name .. "_flipped.tga", tex_name .. ".dds"))

        trim_dds(tex_name, dds_name)

        convert_fnt(fnt1_name, fnt2_name)
    end
end
