# drox.font.generator

Требуются следующие сторонние утилиты:

- BMFont 1.14 (https://www.angelcode.com/products/bmfont/)
- LuaJIT-2.1.0-beta3 (https://luajit.org/download.html)
- ImageMagick 7.1.0 (https://imagemagick.org/script/download.php)
- NVTT 2.1.2 (https://github.com/castano/nvidia-texture-tools/releases)

Всё скачать/распаковать, скопировать в каталог `tools` следующие файлы:
- nvtt.dll
- bmfont64.exe
- luajit.exe
- magick.exe
- nvcompress.exe

Запустить из корня `make_fonts.cmd`, в каталоге _out должны появиться готовые шрифты. Для замены шрифта и/или размеров отредактировать файл `lua/inc_fonts.lua`
